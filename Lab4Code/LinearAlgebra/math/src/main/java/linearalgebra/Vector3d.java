// Samin Ahmed 2043024

package linearalgebra; 

public class Vector3d {
    private double x; 
    private double y;
    private double z; 

    // Constructors
    public Vector3d(double x, double y, double z){
        this.x = x; 
        this.y = y; 
        this.z = z; 
    
    }

    // Getters
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    // Custom
    public double magnitude() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public double dotProduct(Vector3d diff) {
        return x * diff.x + y * diff.y + z * diff.z;
    }

    public Vector3d add(Vector3d diff) {
        double newX = this.x + diff.x;
        double newY = this.y + diff.y;
        double newZ = this.z + diff.z;
        return new Vector3d(newX, newY, newZ);
    }

}
