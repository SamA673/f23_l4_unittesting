// Samin Ahmed

package linearalgebra;
import static org.junit.Assert.*; 

import org.junit.Test;

public class Vector3dTests {

    @Test
    public void testVectorCreationAndAccessors() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(1.0, vector.getX(), 0.1);
        assertEquals(2.0, vector.getY(), 0.1);
        assertEquals(3.0, vector.getZ(), 0.1);
    }

    @Test
    public void testMagnitude() {
        Vector3d vector = new Vector3d(3, 4, 5);
        assertEquals("Should be 50", Math.sqrt(50), vector.magnitude(), 0.1);
    }

    @Test
    public void testDotProduct() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);
        double result = vector1.dotProduct(vector2);
        assertEquals("Expecting 32", 32.0, result, 0.1);
    }

    @Test
    public void testVectorAddition() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);
        Vector3d result = vector1.add(vector2);
        assertEquals(5, result.getX(), 0.1);
        assertEquals(7.0, result.getY(), 0.1);
        assertEquals(9.0, result.getZ(), 0.1);
    }
}